#include "trajectory_generation.h"

Polynomial::Polynomial(){};

Polynomial::Polynomial(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO initialize the object polynomial coefficients 
  update(piIn,pfIn,DtIn);
};

void Polynomial::update(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO update polynomial coefficients
  a[0] = piIn;
  a[1] = 0;
  a[2] = 0;
  a[3] = ((10/pow(DtIn,3))*(pfIn-piIn));
  a[4] = ((-15/pow(DtIn,4))*(pfIn-piIn));
  a[5] = ((6/pow(DtIn,5))*(pfIn-piIn));
};

const double  Polynomial::p     (const double &t){
  //TODO compute position
  double position;
  position = a[0]+a[3]*pow(t,3)+a[4]*pow(t,4)+a[5]*pow(t,5); 
  //std::cout << t << position ;
  return position;
};

const double  Polynomial::dp    (const double &t){
  //TODO compute velocity
  double velocity;
  velocity = a[1]+(2*a[2]*t)+(3*a[3]*pow(t,2))+(4*a[4]*pow(t,3))+(5*a[5]*pow(t,4));
  //std::cout <<  velocity << std :: endl;
  return velocity;
};

Point2Point::Point2Point(const Eigen::Vector2d & X_i, const Eigen::Vector2d & X_f, const double & DtIn)
{
  polx.update(X_i(0),X_f(0),DtIn);
  poly.update(X_i(1),X_f(1),DtIn);
  //TODO initialize object and polynomials
};

Eigen::Vector2d Point2Point::X(const double & time){
  //TODO compute cartesian position
  Eigen::Vector2d cart_position;
  /*cart_position.x() = (X_i.x())+((10/pow(time,3))-(15/pow(time,4))+(6/pow(time,5)))*(X_f.y()-X_i.x());
  cart_position.y() = (X_i.y())+((10/pow(time,3))-(15/pow(time,4))+(6/pow(time,5)))*(X_f.y()-X_i.y());
  */
  cart_position(0) = polx.p(time);
  cart_position(1) = poly.p(time);
  //std::cout<< time << cart_position.x() << cart_position.y() << std :: endl;
  return cart_position;
};

Eigen::Vector2d Point2Point::dX(const double & time){
  //TODO compute cartesian velocity
  Eigen::Vector2d cart_velocity;
  cart_velocity(0) = polx.dp(time);
  cart_velocity(1) = poly.dp(time);
  return cart_velocity;
};