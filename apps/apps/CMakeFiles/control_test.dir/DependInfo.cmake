
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/anis/POO/planar_robot/apps/control/control_test.cpp" "apps/CMakeFiles/control_test.dir/control/control_test.cpp.o" "gcc" "apps/CMakeFiles/control_test.dir/control/control_test.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/anis/POO/planar_robot/apps/src/CMakeFiles/control.dir/DependInfo.cmake"
  "/home/anis/POO/planar_robot/apps/src/CMakeFiles/trajectory_generation.dir/DependInfo.cmake"
  "/home/anis/POO/planar_robot/apps/src/CMakeFiles/kinematic_model.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
