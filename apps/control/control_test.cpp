#include "control.h"
#include "trajectory_generation.h"
#include <iostream>

int main(){

  double t = 0;
  double tf = 10.0;
  double dt = 0.01;
  

  Eigen::Vector2d X_actual ;
  Eigen::Vector2d X_initial ;
  Eigen::Vector2d X_desired ;
  const Eigen::Vector2d X_Final (0.0, 0.6);
  Eigen::Matrix2d Jacc ; 
  Eigen::Vector2d Q_joint (M_PI_2, M_PI_4);
  Eigen::Vector2d Velo ;
  Eigen::Vector2d Velo_des;
  
  RobotModel My_Robot(0.4,0.5);
  Controller My_Controller(My_Robot);
  My_Robot.FwdKin(X_initial,Jacc,Q_joint);
  Point2Point My_position(X_initial,X_Final,tf);
  //std::cout << " t, X actuel, X desired, X actuel, X desired, Q1, Q2" << std::endl;
  while (t <= tf)
  {
    X_desired = My_position.X(t);
    Velo_des = My_position.dX(t);
    Velo = My_Controller.Dqd(Q_joint,X_desired,Velo_des);
    Q_joint = Q_joint + Velo * dt;
    t = t + dt;
    My_Robot.FwdKin(X_actual,Jacc,Q_joint);
    std::cout << t << " " << X_actual(0) << " " << X_desired(0) << " " << X_actual(1) << " " << X_desired(1) << " " << Q_joint(0) << " " << Q_joint(1) << std::endl;
    
  }




  // Show using all three libraries, simulate the motion of a planar robot
  // For a initial position 
  //      q   = M_PI_2, M_PI_4
  // and  
  //      l1  = 0.4, l2 = 0.5
  // simulate a motion achieving 
  //      Xf  = 0.0, 0.6
}