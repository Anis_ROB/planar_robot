#include "kinematic_model.h"
#include <iostream>
#include <cmath>

int main(){
  using namespace std;
  // Compute the forward kinematics and jacobian matrix for 
  double L1 = 0.5;
  double L2 = 0.5;
  Eigen::Vector2d xOut;
  Eigen::Matrix2d JOut;
  Eigen::Vector2d qIn;
  qIn(0)=0; 
  qIn(1)=M_PI/4;
  RobotModel Rm (L1,L2);

  Rm.FwdKin(xOut,JOut,qIn);
  /*cout << xOut(0) << " Then " << xOut(1) << endl;
  cout << JOut(0,0) << " Then " << JOut(0,1) << endl;
  cout << JOut(1,0) << " Then " << JOut(1,1) << endl;*/

  // For a small variation of q, compute the variation on X and check dx = J . dq  
  Eigen::Vector2d xOutST;
  Eigen::Matrix2d JOutST;
  Eigen::Vector2d qInST;
  qInST(0)=0.01; 
  qInST(1)=M_PI/4+0.01;
  Rm.FwdKin(xOutST,JOutST,qInST);
  /*cout << xOutST(0) << " Then " << xOutST(1) << endl;
  cout << JOutST(0,0) << " Then " << JOutST(0,1) << endl;
  cout << JOutST(1,0) << " Then " << JOutST(1,1) << endl;*/

  Eigen::Vector2d Delta_x;
  Delta_x = xOutST - xOut;
  cout << Delta_x(0) << " Then " << Delta_x(1) << endl;

  Eigen::Vector2d Var (0.01,0.01);
  Eigen::Vector2d Delta_J;
  Delta_J = JOut * Var;
  cout << Delta_J(0) << " Then " << Delta_J(1) << endl;
  





}

